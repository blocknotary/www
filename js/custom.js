$(document).ready(function() {
	var partnerLogo_off = [];
	var partnerLogo_on = [];
	var contactUs = $('#contactUs');
	var home = $('#home');
	var logoImgC = $('.logoImgC');
	var submitButton = $('#submit');
	var deviceButton = $('.deviceButton');
	var device_slide_1 = $('#device_slide_1');
	var device_slide_2 = $('#device_slide_2');
	var device_slide_3 = $('#device_slide_3');
	var device_slide_4 = $('#device_slide_4');

	deviceButton.click(function(e) {
		var curId = $(this).attr("id");
		if (curId.indexOf("Hidden") > -1) {
			deviceButton.addClass("hidden");
			$("#" + curId.replace("Hidden","")).removeClass("hidden");
			if (curId.indexOf("ios") > -1) {
				$("#" + curId.replace("ios","android")).removeClass("hidden");
				device_slide_1.attr("src","assets/custom/block5_iphone_1.png");
				device_slide_2.attr("src","assets/custom/block5_iphone_2.png");
				device_slide_3.attr("src","assets/custom/block5_iphone_3.png");
				//device_slide_4.attr("src","assets/custom/block5_iphone_4.png");
			} else {
				$("#" + curId.replace("android","ios")).removeClass("hidden");
				device_slide_1.attr("src","assets/custom/block5_android_1.png");
				device_slide_2.attr("src","assets/custom/block5_android_2.png");
				device_slide_3.attr("src","assets/custom/block5_android_3.png");
				//device_slide_4.attr("src","assets/custom/block5_android_4.png");
			}
		}
	});

	for (var i = 1; i <=18; i++) {
		partnerLogo_off[i] = $("#partnerLogo" + i + "_off");
		partnerLogo_on[i] = $("#partnerLogo" + i + "_on");
		partnerLogo_off[i].hover(function(e) {
			var partnerLogo_on = $("#" + $(this).attr("id").replace("off","on"));
	        partnerLogo_on.removeClass('hidden');
	        $(this).hide();
	        partnerLogo_on.show();
	    }, function() {
	    	var partnerLogo_on = $("#" + $(this).attr("id").replace("off","on"));
	        $(this).hide();
	        partnerLogo_on.show();
	        if (!partnerLogo_on.is(':hover')) {
		    	partnerLogo_on.hide();
	        	$(this).show();
		    }
	    })
	    partnerLogo_on[i].hover(function(e) {

	    }, function() {
	    	var partnerLogo_off = $("#" + $(this).attr("id").replace("on","off"));
	        $(this).hide();
	        partnerLogo_off.show();
	    })
	}

	contactUs.click(function(e) {
		$("html, body").animate({ scrollTop: $(document).height() }, 1000);
	});

	home.click(function(e) {
		location.reload();
	});

	logoImgC.click(function(e) {
		location.reload();
	});

	submitButton.click(function(e) {
		console.log($("#email55").val());
		console.log($("#company55").val());
		console.log($("#name55").val());
		console.log($("#message55").val());
		var data = {
			"from": $("#email55").val(),
			"company": $("#company55").val(),
		 	"name": $("#name55").val(),
		 	"message": $("#message55").val(),
		 	"token": "fc09862e-2f6f-488d-a712-495ff54407b9"
		};
		console.log(data);
		var contactForm = $( "#contact-form" );
		console.log(contactForm);
    	if ($("#email55").val() && $("#company55").val() && $("#name55").val() && $("#message55").val()) {
    		$.ajax({
			    url:"https://bnlandingpage.herokuapp.com/contactUS",
			    //dataType: 'jsonp',
			    method: 'POST',
			    crossDomain: true,
            	//dataType: "json",
			    data: data,
			    success:function(resp){
			    	console.log(resp);
			    	$("#email55").val("");
	            	$("#company55").val("");
		        	$("#name55").val("");
		        	$("#message55").val("");
			    	/*if (resp) {
			    		console.log("Success");
				        swal({   
			                title: "Success",   
			                text: "Your message successfully sent",   
			                type: "success"
			            }, function() {
			            	$("#email55").val("");
			            	$("#company55").val("");
				        	$("#name55").val("");
				        	$("#message55").val("");
			            });
			    	} else {
			    		console.log("Error");
				    	swal({   
			                title: "Error",   
			                text: "Something went wrong. Please, try later or send email directly to hello@blocknotary.com.",   
			                type: "error"
			            });
			    	}*/
			    },
			    error:function(e) {
		    		console.log(e);
			    	console.log("Error");
			    	/*swal({   
		                title: "Error",   
		                text: "Something went wrong. Please, try later or send email directly to hello@blocknotary.com.",   
		                type: "error"
		            });*/
			    }      
			});
			swal({   
			    title: "Success",   
			    text: "Your message successfully sent",   
			    type: "success"
			}, function() {
				/*$("#email55").val("");
				$("#company55").val("");
				$("#name55").val("");
				$("#message55").val("");*/
			});
    	}
	});
});